#!/bin/sh

langunion_com=(langunion.com www.langunion.com)
server_langunion_com=(server.langunion.com www.server.langunion.com)
babbeln_langunion_com=(babbeln.langunion.com www.babbeln.langunion.com)
babbeln_app=(babbeln.app www.babbeln.app)
school_langunion_com=(school.langunion.com www.school.langunion.com)

_warn() {
    message=$1
    echo -e "\033[1;33m"
    echo $message
    echo -e "\033[0m"
}

_unroot_data() {
    ## unroot certbot
    docker run --rm -it -v ./data/certbot:/data alpine chown -R 1000:1000 /data
}

start_reverse_proxy() {
    _warn "### Recreate reverse-proxy container ..."
    docker-compose up --force-recreate -d reverse-proxy
}

update_reverse_proxy() {
    _warn "### Reloading reverse-proxy's nginx ..."
    docker-compose exec reverse-proxy nginx -s reload
}

download_ssl_conf() {
    _unroot_data
    if [ ! -e "./data/certbot/letsencrypt/options-ssl-nginx.conf" ] || [ ! -e "./data/cerbot/letsencrypt/ssl-dhparams.pem" ]; then
        _warn "### Downloading recommended TLS parameters ..."
        mkdir -p "./data/certbot/letsencrypt"
        curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot-nginx/certbot_nginx/_internal/tls_configs/options-ssl-nginx.conf > "./data/certbot/letsencrypt/options-ssl-nginx.conf"
        curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot/certbot/ssl-dhparams.pem > "./data/certbot/letsencrypt/ssl-dhparams.pem"
    fi
}

create_dummy_certs() {
    domains=$1
    host_path_domain="./data/certbot/letsencrypt/live/$domains"
    docker_path_domain="/etc/letsencrypt/live/$domains"
    _warn "### Creating dummy certificate for $domains ..."

    _unroot_data
    mkdir -p "$host_path_domain"

    docker-compose run \
                   --rm \
                   --entrypoint "\
                   openssl req -x509 -nodes -newkey rsa:4096 -days 1\
                           -keyout '$docker_path_domain/privkey.pem' \
                           -out '$docker_path_domain/fullchain.pem' \
                           -subj '/CN=localhost'
                  "\
                   certbot
}

create_dummy_certs_all() {
    create_dummy_certs $langunion_com
    create_dummy_certs $server_langunion_com
    create_dummy_certs $babbeln_langunion_com
    create_dummy_certs $babbeln_app
    create_dummy_certs $school_langunion_com
}

delete_certs() {
    _unroot_data
    domains=$1
    _warn "### Deleting certificates of $domains ..."
    rm -Rf ./data/certbot/letsencrypt/live/$domains
    rm -Rf ./data/certbot/letsencrypt/archive/$domains
    rm -Rf ./data/certbot/letsencrypt/renewal/$domains.conf
}

delete_certs_all() {
    delete_certs $langunion_com
    delete_certs $server_langunion_com
    delete_certs $babbeln_langunion_com
    delete_certs $babbeln_app
    delete_certs $school_langunion_com
}

letsencrypt() {
    _warn "Letsencrypt: requesting certs for $1"

    ## create domain args for certbot like:
    ## -d domain_1 -d domain_2
    local args=("$@")
    domains_arg=""
    for arg in "${args[@]}"; do
        domains_arg="$domains_arg -d $arg"
    done

    echo "$domains_arg"

    docker-compose run --rm --entrypoint \
                   "\
                    certbot certonly --force-renewal \
                            --email sprachschule.maeurer@pm.me --agree-tos --no-eff-email \
                            --webroot --webroot-path=/var/www/ \
                            $domains_arg
                   "\
                   certbot
}

letsencrypt_all() {
    letsencrypt ${langunion_com[0]} ${langunion_com[1]}
    letsencrypt ${server_langunion_com[0]} ${server_langunion_com[1]}
    letsencrypt ${babbeln_langunion_com[0]} ${babbeln_langunion_com[1]}
    letsencrypt ${babbeln_app[0]} ${babbeln_app[1]}
    letsencrypt ${school_langunion_com[0]} ${school_langunion_com[1]}
}

letsencrypt_staging() {
    _warn "Letsencrypt: requesting certs for $1"

    ## create domain args for certbot like:
    ## -d domain_1 -d domain_2
    local args=("$@")
    domains_arg=""
    for arg in "${args[@]}"; do
        domains_arg="$domains_arg -d $arg"
    done

    echo "$domains_arg"

    docker-compose run --rm --entrypoint \
                   "\
                    certbot certonly --force-renewal --staging \
                            --email sprachschule.maeurer@pm.me --agree-tos --no-eff-email \
                            --webroot --webroot-path=/var/www/ \
                            $domains_arg
                   "\
                   certbot
}

letsencrypt_staging_all() {
    letsencrypt_staging ${langunion_com[0]} ${langunion_com[1]}
    letsencrypt_staging ${server_langunion_com[0]} ${server_langunion_com[1]}
    letsencrypt_staging ${babbeln_langunion_com[0]} ${babbeln_langunion_com[1]}
    letsencrypt_staging ${babbeln_app[0]} ${babbeln_app[1]}
    letsencrypt_staging ${school_langunion_com[0]} ${school_langunion_com[1]}
}
