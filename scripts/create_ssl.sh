#!/bin/sh
source ./scripts/lib.sh

download_ssl_conf
delete_certs_all
create_dummy_certs_all
start_reverse_proxy
delete_certs_all
letsencrypt_all
update_reverse_proxy
