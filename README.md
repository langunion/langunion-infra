# Langunion Infrastructure

This project uses docker and co. to setup the whole langunion infrastructure easily.

## make scripts

| script                    | description                             |
|---------------------------|-----------------------------------------|
| `make build_latest_<APP>` | Build the latest docker image for <APP> |
| `make infra_create`       | Create the infra (db, ssl certs, etc.)  |
| `make infra_up`           | Start all services                      |
| `make infra_down`         | Stop all services                       |
| `make infra_update_certs` | Update all ssl certs                    |
| `make infra_update_apps`  | Recreate all containers with new images |
| `make infra_logs`         | Show all logs                           |

## How to guide

Some guides for server maintenance and secruity

## Hardening the server

### Setup Firewall

```bash
pacman - S ufw
```

```bash
ufw default deny
ufw allow <RANDOM SSH PORT>
ufw limit <RANDOM SSH PORT>
ufw allow http
ufw allow https
```

```bash
ufw reload
ufw enable
systemctl enable ufw
systemctl start ufw
```

### Setup fail2ban

```bash
pacman -S fail2ban
systemctl enable fail2ban
systemctl start fail2ban
```

Edit `/etc/fail2ban/jail.local`:

```
[DEFAULT]
bantime = 1d

[sshd]
enabled = true
filter = sshd
banaction = ufw
backend = systemd
maxretry = 5
findtime = 1d
bantime = 2w
ignoreip = 127.0.0.1/8
```

```bash
systemctl restart fail2ban
```

### Change SSH port

```bash
vim /etc/ssh/sshd_config ## change PORT 22 to <RANDOM>
systemctl restart sshd
```

### Setup a sudo user and disable root

```bash
pacman -S sudo
EDITOR=vim visudo ## enable sudo
useradd -m <USERNAME> -G wheel
passwd <USERNAME>
passwd -l root
```

### Health status script

Add this script to check the status of the server

```bash
fail2ban-client status sshd
fail2ban-client banned
```

## Use ssh key for login

On the user's machine:

```bash
ssh-keygen -t ed25519 -C "<YOUR NAME>@langunion"
cat ~/.ssh/id_ed25519.pub
```

Please use a passphrase for more security.
This will generate a priv and pub key

On the server then execute this to add the public key:

```bash
vim .ssh/authorized_keys ## add the public key of the user as new line
```

When you have more than one key on your own machine then do this:


```bash
vim ~/.ssh/config
```

and add something like this:

```
Host langunion
    HostName ssh.langunion.com
    Port <RANDOM PORT>
    IdentityFile ~/.ssh/<NAME OF PRIV SSH KEY>
    User <USER TO LOGIN TO>
    IdentitiesOnly yes
```

Now you can:

```bash
ssh langunion
```

## Setup yubikey for login

Basically the same as normal ssh key login.

You probably want to disable OTP because its annoying if you don't use it at all

```bash
pacman -S yubikey-manager
systemctl enable pcscd
systemctl start pcscd

ykman config usb -d OTP ## disable OTP
```

Generate a ssh key with the yubikey added encryption

```bash
pacamn -S libfido2
ssh-keygen -t ed25519-sk -C "<YOUR NAME>@langunion/w.yubikey"
cat ~/.ssh/id_ed25519_sk.pub
```

Please use a passphrase for more security.
This will generate a priv and pub key

On the server then execute this to add the public key:

```bash
vim .ssh/authorized_keys ## add the public key of the user as new line
```

When you have more than one key on your own machine then do this:


```bash
vim ~/.ssh/config
```

and add something like this:

```
Host langunion
    HostName ssh.langunion.com
    Port <RANDOM PORT>
    IdentityFile ~/.ssh/<NAME OF PRIV SSH KEY>
    User <USER TO LOGIN TO>
    IdentitiesOnly yes
```

Now you can:

```bash
ssh langunion
```
