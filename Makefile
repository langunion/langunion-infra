build_latest: build_latest_server build_latest_babbeln build_latest_website
build_latest_server:
	@echo "Build latest langunion-server docker image"
	@docker build \
		--no-cache \
		-t langunion-server:latest \
		-f ./images/langunion-server \
		./configs/
build_latest_babbeln:
	@echo "Build latest langunion-babbeln docker image"
	@docker build \
		--no-cache \
		-t langunion-babbeln:latest \
		-f ./images/langunion-babbeln \
		./configs/
build_latest_school:
	@echo "Build latest langunion-school docker image"
	@docker build \
		--no-cache \
		-t langunion-school:latest \
		-f ./images/langunion-school \
		./configs/
build_latest_website:
	@echo "Build latest langunion-website docker image"
	@docker build \
		--no-cache \
		-t langunion-website:latest \
		-f ./images/langunion-website \
		./configs/

infra_create:
	@bash ./scripts/create_ssl.sh
	@docker-compose up -d
infra_up:
	@docker-compose up -d 
infra_down:
	@docker-compose down
infra_update_cert:
	@bash ./scripts/update_ssl.sh
	@docker-compose up -d --force-recreate
infra_update_apps:
	@docker-compose up -d --force-recreate
infra_logs:
	@docker-compose logs


## Obsolete commands
test_latest_server:
	@docker run --name langunion_server \
		--network host \
		--rm -d \
		langunion-server:latest
test_latest_babbeln:
	@docker run --name langunion_babbeln \
		--network host \
		--rm -d \
		langunion-babbeln:latest
test_latest_website:
	@docker run --name langunion_website \
		--network host \
		--rm -d \
		langunion-website:latest
test_latest_school:
	@docker run --name langunion_school \
		--network host \
		--rm -d \
		langunion-school:latest
test_latest_db:
	@docker run --name langunion_db \
		--env MARIADB_RANDOM_ROOT_PASSWORD=1 \
		--network host \
		--rm -d \
		langunion-db:latest
